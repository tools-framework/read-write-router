# read-write-router

作者:Allen Yan

#### 介绍
基于springboot的读写分离工具，支持一主N从
支持自定义实现负载均衡策略（默认轮询）
老项目，新项目，完美兼容，不需要改动代码


源码test下有测试用例和示例
#### 软件架构
软件架构说明


#### 安装教程

1. 下载工程
2. 打成jar
3. 引入你自己的工程

#### 使用说明

1. 约定
    数据访问层（DAO）必须符合以下结构
    包名：*..dao.. （包路径里必须包含dao）
    类名：*Mapper（数据访问对象必须以Mapper结尾）
    写库拦截方法名规则：
        insert*(..)
        add*(..)
        save*(..)
        update*(..)
        edit*(..)
        delete*(..)
        remove*(..)
    读库拦截方法名字规则：
        select*(..)
        query*(..)
        get*(..)
        
2. 配置

示例如下：
注意type:write/read

`
dynamic:
      datasource:
        master:
          type:write
          url:jdbc:mysql://127.0.0.1:3306/test-master?useUnicode=true&characterEncoding=utf8&characterSetResults=utf8&useSSL=false
          username:master
          password:123456
        slave1:
          type:read
          url:jdbc:mysql://127.0.0.1:3306/test-slave1?useUnicode=true&characterEncoding=utf8&characterSetResults=utf8&useSSL=false
          username:readonly1
          password:123456
        slave2:
          type:read
          url:jdbc:mysql://127.0.0.1:3306/test-slave2?useUnicode=true&characterEncoding=utf8&characterSetResults=utf8&useSSL=false
          username:readonly1
          password:123456
`