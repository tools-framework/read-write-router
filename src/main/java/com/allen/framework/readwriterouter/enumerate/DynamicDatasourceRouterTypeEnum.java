package com.allen.framework.readwriterouter.enumerate;

/**
 * @Author: Allen
 * @Date: Created in 16:00 2019-08-28
 * @Description:
 */
public enum DynamicDatasourceRouterTypeEnum {

    READ("read" ,"读取数据"),WRITE("write" ,"修改数据（新增，修改，删除）");

    private String code;
    private String desc;

    DynamicDatasourceRouterTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static DynamicDatasourceRouterTypeEnum getByCode(String code) {
        for (DynamicDatasourceRouterTypeEnum dynamicDatasourceRouterTypeEnum : values()) {
            if (dynamicDatasourceRouterTypeEnum.getCode().equals(code)) {
                return dynamicDatasourceRouterTypeEnum;
            }
        }
        return null;
    }


}




