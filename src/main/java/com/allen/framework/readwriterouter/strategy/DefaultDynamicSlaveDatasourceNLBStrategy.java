package com.allen.framework.readwriterouter.strategy;

import com.allen.framework.readwriterouter.IDynamicSlaveDatasourceNLBStrategy;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: Allen
 * @Date: Created in 16:38 2019-08-29
 * @Description: 默认的负载实现，轮询的方式
 */
@Slf4j
public class DefaultDynamicSlaveDatasourceNLBStrategy implements IDynamicSlaveDatasourceNLBStrategy {

    private AtomicInteger slaveCounter = new AtomicInteger(0);

    public String concurrentSlave(List<String> slaveList) {
        log.debug("start default NLB...slave db one by one");
        int index = slaveCounter.getAndIncrement() % slaveList.size();
        return slaveList.get(index);
    }

}
