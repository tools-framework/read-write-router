package com.allen.framework.readwriterouter.properties;

import com.allen.framework.readwriterouter.enumerate.DynamicDatasourceRouterTypeEnum;
import lombok.Data;

/**
 * @Author: Allen
 * @Date: Created in 16:07 2019-08-28
 * @Description:
 */
@Data
public class DynamicDBConfiguration {
    /**
     * 这里区分的数据源的类型，填写一下枚举的code即可
     * @see DynamicDatasourceRouterTypeEnum
     */
    private String type;
    private String url;
    private String username;
    private String password;

    @Override
    public String toString() {
        return "DynamicDBConfiguration{" +
                "type='" + type + '\'' +
                ", url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
