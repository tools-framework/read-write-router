package com.allen.framework.readwriterouter.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author: Alldatasourceen
 * @Date: Created in 16:02 2019-08-28
 * @Description: 动态数据源配置类
 */
@Data
@Component
@ConfigurationProperties(prefix = "dynamic")
public class DynamicDatasourceConfigurationProperties {

    private Map<String, String> datasource;

}
