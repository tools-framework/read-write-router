package com.allen.framework.readwriterouter.aspect;

import com.allen.framework.readwriterouter.datasource.DynamicDatasourceHolder;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @Author: Allen
 * @Date: Created in 09:33 2019-08-29
 * @Description:
 */
@Aspect
@Component
public class DynamicDbRouterAspect {

    private DynamicDatasourceHolder dynamicDatasourceHolder;

    public DynamicDbRouterAspect(DynamicDatasourceHolder dynamicDatasourceHolder) {
        this.dynamicDatasourceHolder = dynamicDatasourceHolder;
    }

    @Pointcut("execution(* *..dao..*Mapper.select*(..))" +
            "|| execution(* *..dao..*Mapper.query*(..))" +
            "|| execution(* *..dao..*Mapper.get*(..))")
    public void readPointcut() {

    }

    @Pointcut("execution(* *..dao..*Mapper.insert*(..))" +
            "|| execution(* *..dao..*Mapper.add*(..))" +
            "|| execution(* *..dao..*Mapper.save*(..))" +
            "|| execution(* *..dao..*Mapper.update*(..))" +
            "|| execution(* *..dao..*Mapper.edit*(..))" +
            "|| execution(* *..dao..*Mapper.delete*(..))" +
            "|| execution(* *..dao..*Mapper.remove*(..))")
    public void writePointcut() {

    }

    @Before("readPointcut()")
    public void read() {
        dynamicDatasourceHolder.nowSlave();
    }

    @Before("writePointcut()")
    public void write() {
        dynamicDatasourceHolder.nowMaster();
    }


}
