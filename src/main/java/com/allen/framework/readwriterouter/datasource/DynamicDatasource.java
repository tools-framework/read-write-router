package com.allen.framework.readwriterouter.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @Author: Allen
 * @Date: Created in 09:58 2019-08-29
 * @Description:
 */
public class DynamicDatasource extends AbstractRoutingDataSource {

    private DynamicDatasourceHolder dynamicDatasourceHolder;

    public DynamicDatasource(DynamicDatasourceHolder dynamicDatasourceHolder) {
        this.dynamicDatasourceHolder = dynamicDatasourceHolder;
    }

    @Override
    protected Object determineCurrentLookupKey() {
        return dynamicDatasourceHolder.getConcurrent();
    }
}
