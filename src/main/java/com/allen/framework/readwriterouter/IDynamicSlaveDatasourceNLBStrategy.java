package com.allen.framework.readwriterouter;

import java.util.List;

/**
 * @Author: Allen
 * @Date: Created in 16:32 2019-08-29
 * @Description: NLB负载接口，支持重新实现，参考DefaultDynamicSlaveDatasourceNLBStrategy
 */
public interface IDynamicSlaveDatasourceNLBStrategy {

    String concurrentSlave(List<String> slaveLists);

}
