package com.allen.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Allen
 * @Date: Created in 10:30 2019-08-30
 * @Description:
 */
@SpringBootApplication
public class ReadWriteRouterApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReadWriteRouterApplication.class, args);
    }

}
