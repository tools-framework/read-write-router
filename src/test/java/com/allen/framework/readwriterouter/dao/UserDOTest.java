package com.allen.framework.readwriterouter.dao;

import lombok.Data;

/**
 * @Author: Allen
 * @Date: Created in 11:32 2019-08-29
 * @Description:
 */
@Data
public class UserDOTest {

    private Long id;

    private String username;

    private String password;

    @Override
    public String toString() {
        return "UserDOTest{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
