package com.allen.framework.readwriterouter.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: Allen
 * @Date: Created in 11:31 2019-08-29
 * @Description:
 */
@Mapper
public interface UserMapper {

    void save(UserDOTest userDOTest);

    UserDOTest select(Long id);

}
