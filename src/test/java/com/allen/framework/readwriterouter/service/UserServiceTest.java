package com.allen.framework.readwriterouter.service;

import com.allen.framework.readwriterouter.dao.UserDOTest;
import com.allen.framework.readwriterouter.dao.UserMapper;
import org.springframework.stereotype.Service;

/**
 * @Author: Allen
 * @Date: Created in 11:30 2019-08-29
 * @Description:
 */
@Service
public class UserServiceTest {

    private UserMapper userMapper;

    public UserServiceTest(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public void save(UserDOTest userDOTest) {
        userMapper.save(userDOTest);
    }

    public UserDOTest selectById(Long id) {
        return userMapper.select(id);
    }

}
